<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="model.Staff"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change your information</title>
</head>
<% 
	String status = (String) request.getAttribute("updateStatus");
	if(status!=null && status.equals("OK")) {
		out.write("alert( " + "' Update succesfully! '" + ");\n");

		request.getRequestDispatcher("viewPersonalInfo.jsp").forward(request, response);
	}
	else {
	Staff staff = (Staff) session.getAttribute("currentStaff");
%>
<body>
<jsp:include page="header.jsp" />
<form action="changePersonalInfo" method="post">

<table>
<tr>
	<td><b>New Full Name:</b></td>
	<td><input type="text" id="newName" name="newName" value="<%=staff.getName() %>"></td>
</tr>
<tr>
	<td><b>Account Name:</b></td>
	<td><%=staff.getAccountName()%></td>
</tr>
<tr>
	<td><b>New Address:</b></td>
	<td><input type="text" name="newAddress" value="<%=staff.getAddress() %>"></td>
</tr>
<tr>
	<td><b>New Email:</b></td>
	<td><input type="text" name="newEmail" value="<%=staff.getEmail() %>"></td>
</tr>
<tr>
	<td><b>New Phone Number:</b></td>
	<td><input type="text" name="newPhoneNumber" value="<%=staff.getPhoneNumber() %>"></td>
</tr>
</table>
<div>
	<input type="submit" name="submit" value="Sumbit">
	<input type="button" onclick="location.href='viewPersonalInfo.jsp'" value="Cancel">
</div>
</form>
</body>
<%} %>
</html>