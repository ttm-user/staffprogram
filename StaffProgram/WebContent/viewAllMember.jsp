<%@page import="java.util.ArrayList"%>
<%@page import="model.Department"%>
<%@page import="model.Staff"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp" />
<% 
	Staff staff = (Staff) session.getAttribute("currentStaff");
	int idDepartment = Integer.parseInt(request.getParameter("idDepartment"));
	Department dpm = new Department(idDepartment);
	ArrayList<Staff> staffArray = dpm.getAllStaffOfDepartment();
%>
<h3 align="center">All Member Of <%=dpm.getDepartmentName() %></h3> <br>
<table align="center" border="1">
<tr>
	<th><b>No.</b></th>
	<th><b>Name</b></th>
	<th><b>Function</b></th>
</tr>
<% 
int i;
for(i=0;i<staffArray.size();i++) { %>
<tr>
	<td width="10%" align="center"><%=i+1 %></td>
	<td width="20%" align="center"><%=staffArray.get(i).getName() %></td>
	<td width="50%" align="center"><%=staffArray.get(i).getPositionOfDepartment(idDepartment)%></td>
	<%
	if(staffArray.get(i).getIdStaff() != staff.getIdStaff()) {
		if(staff.getMaxLevelPosition()==0) { 
	%>
	<td width="10%" align="center"><a href='changePositionOfStaff.jsp?idDepartment=<%=idDepartment%>&idStaff=<%=staffArray.get(i).getIdStaff()%>'>change function</a></td>
	<td width="10%" align="center"><a href='changeDepartmentOfStaff.jsp?idDepartment=<%=idDepartment%>&idStaff=<%=staffArray.get(i).getIdStaff()%>'>change department</a></td>
	<%} else if(staff.getMaxLevelPosition()==1 && staff.checkHeadOfDepartment(idDepartment)) {%>
	<td width="10%" align="center"><a href='changePositionOfStaff.jsp?idDepartment=<%=idDepartment%>&idStaff=<%=staffArray.get(i).getIdStaff()%>'>change function</a></td>
	<%} 
	}
	%>
</tr>
<%} %>
</table>
</body>
</html>