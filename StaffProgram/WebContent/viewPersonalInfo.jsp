<%@page import="model.PositionKind"%>
<%@page import="model.Department"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="model.Staff,model.StaffPosition,java.util.*,model.Department,model.StaffPosition"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp" />
<% Staff staff = (Staff) session.getAttribute("currentStaff");
%>
<center>
<h3>Your Information</h3>
<table>
<tr>
	<td><b>Full Name:</b></td>
	<td><%=staff.getName() %></td>
</tr>
<tr>
	<td><b>Account Name:</b></td>
	<td><%=staff.getAccountName()%></td>
</tr>
<tr>
	<td><b>Password:</b></td>
	<td>********</td>
</tr>
<tr>
	<td><b>Address:</b></td>
	<td><%=staff.getAddress() %></td>
</tr>
<tr>
	<td><b>Email:</b></td>
	<td><%=staff.getEmail()%></td>
</tr>
<tr>
	<td><b>Phone Number:</b></td>
	<td><%=staff.getPhoneNumber()%></td>
</tr>
</table>
You are a
<%
	ArrayList<StaffPosition> spArray = staff.getAllPositionOfStaff();
	for(int i=0;i<spArray.size();i++) {
		int idDepartment = spArray.get(i).getIdDepartment();
		int idPosition = spArray.get(i).getIdPositionKind();
		Department department = new Department(idDepartment);
		PositionKind pos = new PositionKind(idPosition);
		if(pos.getPositionKindName().equals("director")) {
			out.print(pos.getPositionKindName());
		} else {
			out.print(pos.getPositionKindName()+" of the "+department.getDepartmentName());
			if(i!=(spArray.size()-1))
				out.print(" and ");
		}
	}	
%>
<br>
<form>
<input type="button" onclick="location.href='changePassword.jsp'" name="changePass" value="Change Password">
<input type="button" onclick="location.href='changePersonalInfo.jsp'" name="changInfo" value="Change Information">
<input type="button" onclick="location.href='home.jsp'" name="home" value="Back">
</form>
</center>
</body>
</html>