<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp" />
<center>
<h3>Change Password</h3><br>
<%
	String msg = (String)request.getAttribute("changePasswordStatus");
	//out.println(msg);
	if(msg!=null) {
		if(msg.equals("changed successfully")) {
			out.print("<script>alert(\"password changed!\"); window.location=\"viewPersonalInfo.jsp\";</script>");
			//request.getRequestDispatcher("viewPersonalInfo.jsp").forward(request, response);
		}
%>
<p><font color="red"><%=msg %></font></p>
<%	} %>
<form action="changePassword" method="post">
<table>
<tr>
	<td><b>Current Password:</b></td>
	<td><input type="password" name="currPass"></td>
</tr>
<tr>
	<td><b>New Password:</b></td>
	<td><input type="password" name="newPass"></td>
</tr>
<tr>
	<td><b>Confirm Password:</b></td>
	<td><input type="password" name="conPass"></td>
</tr>
</table>
<div>
	<input type="submit" value="OK">
	<input type="button" onclick="location.href='viewPersonalInfo.jsp'" value="Cancel">
</div>
</form>
</center>
</body>
</html>