<%@page import="java.util.ArrayList"%>
<%@page import="model.PositionKind"%>
<%@page import="model.Department"%>
<%@page import="model.StaffFactory"%>
<%@page import="model.Staff"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp" />
<%
	String status = (String) request.getAttribute("changeStatus");
	//out.print(status); 
	if(status == null) {
		Staff staff = (Staff) session.getAttribute("currentStaff");
		int idDepartment = Integer.parseInt(request.getParameter("idDepartment"));
		int idStaff = Integer.parseInt(request.getParameter("idStaff"));
		StaffFactory sFactory = new StaffFactory();
		Staff changeStaff = sFactory.createStaff(idStaff);
		
		Department dpm = new Department(idDepartment);
		PositionKind pos = new PositionKind();
		ArrayList<PositionKind> posArr = pos.getAllPositionName(staff.getMaxLevelPosition()); 
		ArrayList<Department> dpmArr = dpm.getAllDepartmentInfo();
%>
	<h3>
		Change department of
		<%=changeStaff.getName() %>
		in the
		<%=dpm.getDepartmentName() %></h3>
	<br>
	<form action="changeDepartment" method="post">
	<div>
		New position: <select name="new_position">
			<%
		for(int i=0;i<posArr.size();i++) { 
	%>
			<option><%=posArr.get(i).getPositionKindName() %></option>
			<%
		}
	%>
		</select> 
	</div>
	<div>
		New department: <select name="new_department">
			<%
		for(int i=0;i<dpmArr.size();i++) { 
	%>
			<option><%=dpmArr.get(i).getDepartmentName() %></option>
			<%
		}
	%>
		</select>
	</div>
		<input type="hidden" name="changePositionStaffID"
			value="<%=idStaff %>"> <input type="hidden"
			name="idDepartment" value="<%=idDepartment %>"> <br>
		<div>
			<input type="submit" value="Submit"> <input type="button"
				onclick="location.href='viewAllMember.jsp'" value="Cancel">
		</div>
	</form>
	<%
	} 
	else {
		out.print("<script>alert('"+status+"');");
		String idDepartment = request.getAttribute("idDepartment").toString();
		out.print("location.href='viewAllDepartmentInfo.jsp?idDepartment="+idDepartment+"'</script>");
		//request.getRequestDispatcher("viewAllDepartmentInfo.jsp").forward(request, response);
	}
%>
</body>
</html>