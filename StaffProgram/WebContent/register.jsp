<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    var checking_html = 'Checking...';
    var exists_fail = 'All required fields must be filled in.';
    var status = false;


    $("#registerBtn").click(function(){
    	//alert('test');
		if ((!exists($("#fullName").val())) ||
		        (!exists($("#fullName").val())) ||
		        (!exists($("#accountName").val())) ||
		        (!exists($("#password").val())) ||
		        (!exists($("#confirmPassword").val())) ||
		        (!exists($("#email").val())) ||
		        (!exists($("#phoneNumber").val())) ||
		        (!exists($("#address").val())) )  {
		      $("#statusLabel").html(exists_fail);
		      return false;
		   }
		   else if (!verifyEmail($("#email").val())) {
			   $("#statusLabel").html("E-mail Address is not valid.");
			   return false;
		   }
		   else if (!$("#password").val().equals($("#confirmPassword").val()))
		   {
			   $("#statusLabel").html("Passwords do not match.");
			   return false;
		   }
		   else if (!verifyPhone($("#phoneNumber").val())) {
			   $("#statusLabel").html("Phone is not valid.");
			   return false;
		   } else {
			   //document.getElementById('register_frm').submit();
			   return true;
		   }
    });
    

	$("#check_accountName_availability").click(function() {
		var min_chars = 3;  
	    var characters_error = 'Minimum amount of chars is 3';  
	    
	    if(!exists($("#accountName").val())){
	    	$('#check_accountName_result').html(exists_fail);
	    } else if($('#accountName').val().length < min_chars){
			$('#check_accountName_result').html(characters_error);  
		} else {
			$('#check_accountName_result').html(checking_html);  
			check_accountName_availability();
		}
	});
	
	$("#check_email_availability").click(function() {
		if(!exists($("#email").val())) {
			$('#check_email_result').html(exists_fail);
		} else if(!verifyEmail($("#email").val())) {
			$('#check_email_result').html('this email address is not valid.');
		} else {
			$('#check_email_result').html(checking_html);
			check_email_availability();
		}
	});
	
	function exists(inputVal)
	{
	    var result = false;
	    for (var i = 0; i <= inputVal.length; i++) {
	        if ((inputVal.charAt(i) != " ") && (inputVal.charAt(i) != "")) {
	            result = true;
	            break;
	        }
	    }
	    return result;
	}

	function verifyEmail(emailVal)
	{
	    var result = true;
	    var foundAt = false;
	    var foundDot = false;
	    var atPos = -1;
	    var dotPos = -1;
	    for (var i = 0; i < emailVal.length; i++) {
	        if (emailVal.charAt(i) == "@") {
	            foundAt = true;
	            atPos = i;
	        }
	        else if (emailVal.charAt(i) == ".") {
	            foundDot = true;
	            dotPos= i;
	        }
	    }
	    if ((!foundAt) || (!foundDot) || (dotPos < atPos)) {
	        result = false;
	    }
	    return result;
	}

	function verifyPhone(phoneVal)
	{
	    var result = false;
	    var cnt = 0;
	    for (var i = 0; i < phoneVal.length; i++) {
	        if (parseFloat(phoneVal.charAt(i))) {
	            cnt++;
	            if (cnt >= 7) {
	                result = true;
	                break;
	            }
	        }
	    }
	    return result;
	}

	function check_accountName_availability(){  
        //get the username  
  		var username = $('#accountName').val();  
        //use ajax to run the check  
        $.ajax({
        	url: 'check_account_name.jsp',
        	data: 'username='+username,
        	type: 'post',
        	success: function(result){  
                //if the result is 1  
                if(result == 1){  
                    //show that the username is available  
                    $('#check_accountName_result').html(username + ' is available');  
                }else{  
                    //show that the username is NOT available  
                    $('#check_accountName_result').html(username + ' already exists');  
                }  
        	}
        });
	}  
	function check_email_availability(){  
        //get the username  
        var email = $('#email').val();  
  
        //use ajax to run the check  
        $.ajax({
        	url: 'check_email_exist.jsp',
        	data: 'email='+email,
        	type: 'post',
        	success: function(result){  
                //if the result is 1  
                if(result == 1){  
                    //show that the username is available  
                    $('#check_email_result').html('this email is available');  
                }else{  
                    //show that the username is NOT available  
                    $('#check_email_result').html('this email already exists');  
                }  
        	}
        });
	}
});
</script>
</head>
<body>
<h3>Register Form</h3><br>
<%
	String status = (String) request.getAttribute("register_status");
	if(status!=null) {
		if(status.equals("register successfully")){
			out.print("<script>alert('"+status+"');");
			out.print("location.href='login.jsp';</script>");
		} else {
			out.print("<script>alert('"+status+"')</script>");
		}
	} else {
%>
<label id="statusLabel"></label><br>
<form id="register_frm" action="register" method="post">
<table>
<tr>
	<td><b>Full Name:</b></td>
	<td><input id="fullName" type="text" name="fullName"></td>
</tr>
<tr>
	<td><b>Account Name:</b></td>
	<td><input id="accountName" type="text" name="accountName"></td>
	<td><input type="button" id="check_accountName_availability" name="check_accountName_availability" value="Check Availability"></td>
	<td><div id="check_accountName_result"></div></td>
</tr>
<tr>
	<td><b>Password:</b></td>
	<td><input id="password" type="password" name="password"></td>
</tr>
<tr>
	<td><b>Confirm Password:</b></td>
	<td><input id="confirmPassword" type="password" name="confirmPassword "></td>
</tr>
<tr>
	<td><b>Address:</b></td>
	<td><input id="address" type="text" name="address"></td>
</tr>
<tr>
	<td><b>E-mail:</b></td>
	<td><input id="email" type="text" name="email"></td>
	<td><input type="button" id="check_email_availability" value="Check Availability"></td>
	<td><div id="check_email_result"></div>
</tr>
<tr>
	<td><b>Phone Number:</b></td>
	<td><input id="phoneNumber" type="text" name="phoneNumber"></td>
</tr>
</table>
<div>
	<input type="submit" id="registerBtn" value="Register">
	<input type="button" onclick="location.href='login.jsp'" value="Cancel">
</div>
</form>
<%} %>
</body>
</html>