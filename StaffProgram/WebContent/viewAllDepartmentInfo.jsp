<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="model.Department,model.Staff"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
table,th,td
{
border:1px solid black;
}
</style>
<body>
<jsp:include page="header.jsp"></jsp:include>
<% 
	Department dp = new Department();
	ArrayList<Department> dpArray = dp.getAllDepartmentInfo();
	Staff staff = (Staff) session.getAttribute("currentStaff");
	//out.print(staff.getName());
%>
<h3 align="center">Department List</h3> <br>
<table align="center">
<tr>
	<th><b>No.</b></th>
	<th><b>Name</b></th>
	<th><b>Description</b></th>
	<th><b>Head Of Department</b></th>
</tr>
<% 
int i;
for(i=0;i<dpArray.size();i++) { 
String name = dpArray.get(i).getDepartmentName();
String description = dpArray.get(i).getDescription();
String director = dpArray.get(i).getDirector();
%>
<tr>
	<td><%=i+1 %></td>
	<td width="20%" align="center"><% if(name != null) out.print(name); %></td>
	<td width="50%" align="center"><%if(description != null) out.print(description); %></td>
	<td width="10%" align="center"><%if(director != null) out.print(director); %></td>
	<td width="10%" align="center"><a href='viewAllMember.jsp?idDepartment=<%=dpArray.get(i).getIdDepartment()%>'>view members</a></td>
</tr>
<%} %>
</table>

<input type="button" onclick="location.href='home.jsp'" value="Back">
</body>
</html>