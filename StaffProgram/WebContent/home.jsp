<%@page import="model.Staff"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<jsp:include page="header.jsp"/>
<body>
	 <center>	
	 	<h3>Home Page</h3>		
            <form action="home" method="get">
            	<input type="submit"  name="personalInfo" value="Personal Information"> <br>
            	<input type="submit"  name="departmentInfo" value="All Department Information">
            	<% 
            		Staff staff = (Staff) session.getAttribute("currentStaff");
            		int rights = staff.getMaxLevelPosition();
            		if(rights==0) {
            	%>
            	<br>
            	<input type="submit" name="regisManagement" value="Registration Management">
            	<%} %>
            </form>
     </center> 
</body>

