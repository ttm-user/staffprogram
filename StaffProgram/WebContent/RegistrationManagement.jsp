<%@page import="model.PositionKind"%>
<%@page import="model.Department"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Staff"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
	Staff staff = (Staff) session.getAttribute("currentStaff");
	ArrayList<Staff> staffArray = staff.getAllNotActivedStaff();
	if(staffArray==null) {
		out.print("Don not have staff who is not actived.");
	} else {
	Department dpm = new Department();
	PositionKind pos = new PositionKind();
	ArrayList<PositionKind> posArr = pos.getAllPositionName(staff.getMaxLevelPosition()); 
	ArrayList<Department> dpmArr = dpm.getAllDepartmentInfo();
%>
<body>
<center>
<h3>Registration Management</h3>
<form action="registrationManagement" method="get">
<table border="1">
<tr>
	<th>No.</th>
	<th>Full Name</th>
	<th>User Name</th>
	<th>Department</th>
	<th>Position</th>
</tr>
<% 
	int i =0;
	for(i=0;i<staffArray.size();i++) {
		out.print("<tr>");
		out.print("<td>"+(i+1)+"</td>");
		out.print("<td>"+staffArray.get(i).getName()+"</td>");
		out.print("<td>"+staffArray.get(i).getAccountName()+"</td>");
		out.print("<td><select name='new_department'>");
		for(int j=0;j<dpmArr.size();j++){
			out.print("<option>"+dpmArr.get(j).getDepartmentName()+"</option>");
		}
		out.print("</td>");
		out.print("<td><select name='new_position'>");
		for(int j=0;j<posArr.size();j++){
			out.print("<option>"+posArr.get(j).getPositionKindName()+"</option>");
		}
		out.print("</td>");
		out.print("<td><input type='submit' value='Accept'></td>"); 
		out.print("</tr>");
	} 
	}
%>
</table>
</form>
</center>
</body>
</html>