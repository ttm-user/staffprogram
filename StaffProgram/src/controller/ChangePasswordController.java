package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Staff;

public class ChangePasswordController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		String currPass = request.getParameter("currPass");
		String newPass = request.getParameter("newPass");
		String confirmPass = request.getParameter("conPass");
		
		HttpSession session = request.getSession();
		Staff staff = (Staff) session.getAttribute("currentStaff");
		String status = changePassword(staff, currPass, newPass, confirmPass);
		request.setAttribute("changePasswordStatus", status);
		request.getRequestDispatcher("changePassword.jsp").forward(request, response);
		System.out.println("status:"+status);
	}
	
	private String changePassword(Staff staff, String currPass, String newPass, String confirmPass){
		if(checkCurrentPassword(staff, currPass)) {
			if(checkNewPassword(newPass, confirmPass)){
				staff.setPassword(newPass);
				staff.changePassword();
				return "changed successfully";
			}
			else
				return "new password does not match confirm password.";
		} else
			return "current password is incorrect.";
	}
	
	private boolean checkCurrentPassword(Staff staff, String currPass) {
		return staff.getPassword().equals(currPass);
	}
	
	private boolean checkNewPassword(String newPass, String confirmPass) {
		return newPass.equals(confirmPass);
	}
}
