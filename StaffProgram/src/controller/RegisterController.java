package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Staff;
import model.StaffFactory;


public class RegisterController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		String fullName = null, accountName = null, password = null, address = null,
				email = null, phoneNumber = null;
		if(request.getParameter("fullName") != null){
			fullName = (String) request.getParameter("fullName");
		}
		if(request.getParameter("accountName") != null){
			accountName = (String) request.getParameter("accountName");
		}
		if(request.getParameter("password") != null){
			password = (String) request.getParameter("password");
		}
		if(request.getParameter("address") != null){
			address = (String) request.getParameter("address");
		}
		if(request.getParameter("email") != null){
			email = (String) request.getParameter("email");
		}
		if(request.getParameter("phoneNumber") != null){
			phoneNumber = (String) request.getParameter("phoneNumber");
		}
		String status;
		if(fullName != null && accountName != null && password != null && address!=null
				&& email!=null && phoneNumber != null) {
			if(registerNewStaff(fullName, accountName, password, address, email, phoneNumber)){
				status = "register successfully";
			} else
				status = "insert into database fail";
		} else
			status = "registration fail";
		request.setAttribute("register_status", status);
		request.getRequestDispatcher("register.jsp").forward(request, response);
	}

	private boolean registerNewStaff(String fullName, String accountName, String password,
			String address, String email, String phoneNumber){
		StaffFactory factory = new StaffFactory();
		Staff newStaff = factory.createStaff();
		return newStaff.addNewStaff(fullName, accountName, password, address, email, phoneNumber);
	}
}
