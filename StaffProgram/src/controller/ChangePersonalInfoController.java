package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Staff;

public class ChangePersonalInfoController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		HttpSession session =  request.getSession(true);
		Staff staff = (Staff) session.getAttribute("currentStaff");
		String fullName = request.getParameter("newName");
		System.out.println("newName:"+fullName);
		String email = request.getParameter("newEmail");
		String phoneNumber = request.getParameter("newPhoneNumber");
		String address = request.getParameter("newAddress");
		updatePersonalInfo(staff, fullName, email, phoneNumber, address);
		request.setAttribute("updateStatus", "OK");
		request.getRequestDispatcher("changePersonalInfo.jsp").forward(request, response);
	}
	
	private void updatePersonalInfo(Staff staff, String fullName, String email, String phoneNumber, String address){
		staff.setName(fullName);
		staff.setEmail(email);
		staff.setPhoneNumber(phoneNumber);
		staff.setAddress(address);
		staff.updateStaffInfo();
	}
}
