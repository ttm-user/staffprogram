package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Department;
import model.PositionKind;
import model.Staff;
import model.StaffPosition;

public class ChangeDepartmentController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("here");
		HttpSession session = request.getSession();
		Staff staff = (Staff)session.getAttribute("currentStaff");
		int changeStaffID = 0;
		String newPosition = null;
		String newDepartment;
		int oldIdDepartment =0;
		int newIdDepartment = 0;
		String status;
		if(request.getParameter("changePositionStaffID")!=null && request.getParameter("idDepartment")!=null 
				&& request.getParameter("new_position") != null && request.getParameter("new_department") != null) {
			changeStaffID =  Integer.parseInt(request.getParameter("changePositionStaffID"));
			newPosition = request.getParameter("new_position");
			oldIdDepartment =  Integer.parseInt(request.getParameter("idDepartment"));
			newDepartment = request.getParameter("new_department");
			Department dpm = new Department();
			newIdDepartment = dpm.getIdDepartment(newDepartment);
			request.setAttribute("idDepartment", newIdDepartment);
			status = changeDepartment(staff, changeStaffID, oldIdDepartment, newIdDepartment, newPosition);
		} else status = "change fail";
		
		request.setAttribute("changeStatus", status);
		request.getRequestDispatcher("changePositionOfStaff.jsp").forward(request, response);
	}

	private String changeDepartment(Staff staff, int changeStaffID,
			int oldIdDepartment, int newIdDepartment, String newPosition) {
		// TODO Auto-generated method stub
		String status=null;
		int idMaxLevelOfStaff = staff.getMaxLevelPosition();
		PositionKind pos = new PositionKind();
		int idPosition = pos.getIdKind(newPosition);
		if(idMaxLevelOfStaff<idPosition) {
			StaffPosition sPos = new StaffPosition();
			sPos.setIdDepartment(newIdDepartment);
			sPos.setIdStaff(changeStaffID);
			sPos.setIdPositionKind(idPosition);
			ArrayList<String> idColumns = new ArrayList<String>();
			ArrayList<String> id = new ArrayList<String>();
			idColumns.add("idStaff");
			idColumns.add("idDepartment");
			id.add(String.valueOf(changeStaffID));
			id.add(String.valueOf(oldIdDepartment));
			if(sPos.updateInfo(idColumns, id)) {
				status = "update successfully";
			}
			else status = "database connect fail";
		}else status = "you don't have rights to change";
		return status;
	}    
}
