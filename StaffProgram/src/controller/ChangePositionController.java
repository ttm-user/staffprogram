package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.PositionKind;
import model.Staff;
import model.StaffPosition;

public class ChangePositionController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("here");
		HttpSession session = request.getSession();
		Staff staff = (Staff)session.getAttribute("currentStaff");
		int changeStaffID = 0;
		String newPosition = null;
		int idDepartment =0;
		String status;
		if(request.getParameter("changePositionStaffID")!=null && request.getParameter("idDepartment")!=null && request.getParameter("position") != null) {
			changeStaffID =  Integer.parseInt(request.getParameter("changePositionStaffID"));
			newPosition = request.getParameter("position");
			idDepartment =  Integer.parseInt(request.getParameter("idDepartment"));
			request.setAttribute("idDepartment", idDepartment);
			status = changePosition(staff, changeStaffID, idDepartment, newPosition);
		} else status = "change fail";
		
		request.setAttribute("changeStatus", status);
		request.getRequestDispatcher("changePositionOfStaff.jsp").forward(request, response);
	}

	private String changePosition(Staff staff, int changeStaffID,
			int idDepartment, String newPosition) {
		// TODO Auto-generated method stub
		String status=null;
		int idMaxLevelOfStaff = staff.getMaxLevelPosition();
		PositionKind pos = new PositionKind();
		int idPosition = pos.getIdKind(newPosition);
		if(idMaxLevelOfStaff<idPosition) {
			StaffPosition sPos = new StaffPosition();
			sPos.setIdDepartment(idDepartment);
			sPos.setIdStaff(changeStaffID);
			sPos.setIdPositionKind(idPosition);
			ArrayList<String> idColumns = new ArrayList<String>();
			ArrayList<String> id = new ArrayList<String>();
			idColumns.add("idStaff");
			idColumns.add("idDepartment");
			id.add(String.valueOf(changeStaffID));
			id.add(String.valueOf(idDepartment));
			if(sPos.updateInfo(idColumns, id)) {
				status = "update successfully";
			}
			else status = "database connect fail";
		}else status = "you don't have rights to change";
		return status;
	}    
}
