package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.rmi.server.Dispatcher;

public class HomeController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		int rights = (int) session.getAttribute("rights");
		
		if(request.getParameter("personalInfo") != null) {
			request.getRequestDispatcher("viewPersonalInfo.jsp").forward(request, response);
		} else if (request.getParameter("departmentInfo") != null) {
			request.getRequestDispatcher("viewAllDepartmentInfo.jsp").forward(request, response);
		} else if (request.getParameter("regisManagement") != null) {
			if(rights==0)
				request.getRequestDispatcher("registrationManagement.jsp").forward(request, response);
		}
	}

}
