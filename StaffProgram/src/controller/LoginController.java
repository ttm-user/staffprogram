package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.log.SystemLogHandler;

import model.Authenticator;
import model.Staff;
import model.StaffFactory;

public class LoginController extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
 
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        Authenticator authenticator = new Authenticator();
        String result = authenticator.authenticate(username,password);
        if (result.equals("success")) {
        	StaffFactory sFactory = new StaffFactory();
        	Staff staff = sFactory.createStaff(username, password);
        	//System.out.println("id:"+staff.getIdStaff());
        	HttpSession session = request.getSession(true);	    
            session.setAttribute("currentStaff",staff);
            session.setAttribute("rights", staff.getMaxLevelPosition());
            session.setAttribute("username", username);
            response.sendRedirect("home.jsp"); 
        } else {
        	response.sendRedirect("invalidLogin.jsp"); 
        }
    }
}
