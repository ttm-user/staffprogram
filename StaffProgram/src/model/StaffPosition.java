package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StaffPosition {
	public int idStaff;
	public int idPositionKind;
	public int idDepartment;
	DBInterface dbi;
	DBIdentifier dbId;
	
	/*
	public StaffPosition(int idStaff, int idDepartment) {
		this.idStaff = idStaff;
		this.idDepartment = idDepartment;
		dbi = new DBConnector();
		ArrayList<String> getColumns = new ArrayList<String>();
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		getColumns.add("idPositionKind");
		idColumns.add("idStaff");
		idColumns.add("idDepartment");
		id.add(String.valueOf(idStaff));
		id.add(String.valueOf(id));
		ResultSet rs = dbi.readFields("StaffPosition", getColumns, idColumns, id);
		try {
			if(rs.next()) {
				this.idPositionKind = rs.getInt("idPositionKind");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
	public void setIdStaff(int id){
		this.idStaff = id;
	}
	
	public int getIdStaff(){
		return idStaff;
	}
	
	public void setIdPositionKind(int id) {
		this.idPositionKind =id;
	}
	
	public int getIdPositionKind(){
		return idPositionKind;
	}
	
	public void setIdDepartment(int id){
		this.idDepartment =id;
	}
	
	public int getIdDepartment(){
		return idDepartment;
	}
	
	public boolean updateInfo(ArrayList<String> idColumns, ArrayList<String> id){
		dbi = new DBConnector();
		dbId = new DBIdentifier();
		ArrayList<String>  updateColumn = new ArrayList<String>();
		ArrayList<String>  updateInfo = new ArrayList<String>();
		updateColumn.add(dbId.STAFF_ID);
		updateInfo.add(String.valueOf(this.idStaff));
		updateColumn.add(dbId.POSITIONKIND_ID);
		updateInfo.add(String.valueOf(this.idPositionKind));
		updateColumn.add(dbId.DEPARTMENT_ID);
		updateInfo.add(String.valueOf(this.idDepartment));
		
		boolean status = dbi.updateInfo(dbId.STAFFPOSITION_TABLE, idColumns, id, updateColumn, updateInfo);
		dbi.close();
		return status;
	}
}