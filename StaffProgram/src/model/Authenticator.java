package model;

public class Authenticator {
	DBInterface dbi;
	
	public Authenticator() {
		dbi = new DBConnector();
	}
	
	public String authenticate(String username, String password) {
		//MD5Hash md5 = new MD5Hash();
		//String md5Pass = md5.md5Java(password);
		
		int staffNumber = dbi.checkLogin(username, password);
		//System.out.println("number:"+staffNumber);
		dbi.close();
		if(staffNumber == 0)
			return "fail";
		else 
			return "success";		
	}
	
	
}
