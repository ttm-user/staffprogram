package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PositionKind {

	private int idKind;
	private String name;
	private int level;
	DBInterface dbi;
	
	public PositionKind() {
		super();
	}
	public PositionKind(int id){
		this.idKind = id;
		dbi = new DBConnector();
		ResultSet rs = dbi.readAllInfoOfObject(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_ID, String.valueOf(idKind));
		try {
			if(rs.next()) {
				this.name = rs.getString(DBIdentifier.POSITIONKIND_NAME);
				this.level = rs.getInt(DBIdentifier.POSITIONKIND_LEVEL);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		dbi.close();
	}
	
	public void addNewPosition(String newPosition){
		dbi = new DBConnector();
		ArrayList<String> columns = new ArrayList<String>();
		ArrayList<String> insertVal = new ArrayList<String>();
		columns.add(DBIdentifier.POSITIONKIND_NAME);
		insertVal.add(newPosition);
		dbi.insert(DBIdentifier.POSITIONKIND_TABLE, columns, insertVal);
		dbi.close();
	}
	
	public String getPositionKindName(){
		return name;
	}
	
	public int getIdKind(){
		return idKind;
	}
	
	public int getLevel(){
		return level;
	}
	
	public ArrayList<PositionKind> getAllPositionName(int minLevel) {
		dbi = new DBConnector();
		ResultSet rs = dbi.readAllInfoOfTable(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_LEVEL +" > "+minLevel);
		ArrayList<PositionKind> positionName = new ArrayList<PositionKind>();
		try {
			while(rs.next()) {
				PositionKind pos = new PositionKind(rs.getInt(DBIdentifier.POSITIONKIND_ID));
				positionName.add(pos);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbi.close();
		try {
			if(rs != null)
				rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return positionName;
	}
	
	public int getIdKind(String name){
		dbi = new DBConnector();
		return Integer.parseInt(dbi.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_ID, DBIdentifier.POSITIONKIND_NAME, name));
	}
}
