package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Department {
	
	private int idDepartment;
	private String departmentName;
	private String description;
	DBInterface dbi;
	
	public Department(){
	}
	
	public Department(int id){
		this.idDepartment = id;
		dbi = new DBConnector();
		ResultSet rs = dbi.readAllInfoOfObject(DBIdentifier.DEPARTMENT_TABLE, DBIdentifier.DEPARTMENT_ID, String.valueOf(idDepartment));
		try{
			if(rs.next()) {
			this.departmentName = rs.getString(DBIdentifier.DEPARTMENT_NAME);
			this.description = rs.getString(DBIdentifier.DEPARTMENT_DESCRIPTION);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		dbi.close();
	}
	
	public String getDepartmentName() {
		return departmentName;
	}
	
	public String getDescription(){
		return description;
	}
	
	public int getIdDepartment(){
		return idDepartment;
	}
	
	public int getIdDepartment(String name){
		dbi = new DBConnector();
		int id = Integer.parseInt(dbi.readField(DBIdentifier.DEPARTMENT_TABLE, DBIdentifier.DEPARTMENT_ID, DBIdentifier.DEPARTMENT_NAME, name));
		dbi.close();
		return id;
	}
	
	public ArrayList<Department> getAllDepartmentInfo(){
		ArrayList<Department> departArray = new ArrayList<Department>();
		dbi = new DBConnector();
		ResultSet rs = dbi.readAllInfoOfTable(DBIdentifier.DEPARTMENT_TABLE);
		try {
			while(rs.next()) {
				Department dp = new Department(rs.getInt(DBIdentifier.DEPARTMENT_ID));
				departArray.add(dp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		dbi.close();
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return departArray;
	}
	
	public String getDirector(){
		dbi = new DBConnector();
		int idKind = Integer.parseInt(dbi.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_ID, DBIdentifier.POSITIONKIND_LEVEL, "1"));
		System.out.println("idDepartment:"+idDepartment);
		ArrayList<String> getColumns = new ArrayList<String>();
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		
		getColumns.add(DBIdentifier.STAFF_ID);
		idColumns.add(DBIdentifier.POSITIONKIND_ID);
		idColumns.add(DBIdentifier.DEPARTMENT_ID);
		id.add(String.valueOf(idKind));
		id.add(String.valueOf(idDepartment));
		
		ResultSet rs = dbi.readFields(DBIdentifier.STAFFPOSITION_TABLE, getColumns, idColumns, id);
		int idStaff = 0;
		try {
			if(rs.next())
				idStaff = rs.getInt("idStaff");
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		String director = dbi.readField(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_NAME, DBIdentifier.STAFF_ID, String.valueOf(idStaff));
		dbi.close();
		try {
			if(rs != null)
				rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return director;
	}
	
	public ArrayList<Staff> getAllStaffOfDepartment(){
		ArrayList<Staff> staffs = new ArrayList<Staff>();
		dbi = new DBConnector();
		ResultSet rs = dbi.readAllInfoOfObject(DBIdentifier.STAFFPOSITION_TABLE, DBIdentifier.STAFFPOSITION_IDDEPARTMENT, String.valueOf(this.idDepartment));
		
		try {
			while(rs.next()) {
				int idStaff = rs.getInt(DBIdentifier.STAFF_ID);
				StaffFactory sFactory = new StaffFactory(); 
				Staff staff = sFactory.createStaff(idStaff);
				staffs.add(staff);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbi.close();
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return staffs;
	}
}
