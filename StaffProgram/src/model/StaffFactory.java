package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sun.org.apache.regexp.internal.recompile;

public class StaffFactory {
	
	DBInterface dbi;
	
	public Staff createStaff(String username, String password){
		dbi = new DBConnector();
		int idStaff = Integer.parseInt(dbi.readField(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, DBIdentifier.STAFF_ACCOUNTNAME, username));
		ArrayList<String> getColumns = new ArrayList<String>();
		getColumns.add(DBIdentifier.POSITIONKIND_ID);
		ArrayList<String> idColumns = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		ArrayList<String> id = new ArrayList<String>();
		id.add(String.valueOf(idStaff));
		ResultSet rs = dbi.readFields(DBIdentifier.STAFFPOSITION_TABLE, getColumns, idColumns, id);
		int min = 99;
		try {
			while(rs.next()){
				String idKind = rs.getString(DBIdentifier.POSITIONKIND_ID);
				int level = Integer.parseInt(dbi.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_LEVEL, DBIdentifier.POSITIONKIND_ID, idKind));
				if(level<min) min =  level;
			}
			if(min==0)
				return new Director(username, password);
			else if(min==1)
				return new HeadOfOffice(username, password);
			else
				return new Member(username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbi.close();
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Staff createStaff(int idStaff){
		dbi = new DBConnector();
		ArrayList<String> getColumns = new ArrayList<String>();
		getColumns.add(DBIdentifier.POSITIONKIND_ID);
		ArrayList<String> idColumns = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		ArrayList<String> id = new ArrayList<String>();
		id.add(String.valueOf(idStaff));
		ResultSet rs = dbi.readFields(DBIdentifier.STAFFPOSITION_TABLE, getColumns, idColumns, id);
		int min =99;
		try {
				while(rs.next()){
					String idKind = rs.getString(DBIdentifier.POSITIONKIND_ID);
					int level = Integer.parseInt(dbi.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_LEVEL, DBIdentifier.POSITIONKIND_ID, idKind));
					if(level<min) min =  level;
				}
				if(min==0)
					return new Director(idStaff);
				else if(min==1)
					return new HeadOfOffice(idStaff);
				else
					return new Member(idStaff);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbi.close();
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Staff createStaff(){
		return new Member();
	}
}
