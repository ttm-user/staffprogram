package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public abstract class Staff {
	protected int idStaff;
	protected String accountName;
	protected String password;
	protected String fullName;
	protected String phoneNumber;
	protected String email;
	protected String address;
	protected String activedStatus;
	protected int maxlevelPosition;
	DBIdentifier dbId;
	DBInterface db;
	StaffPosition pos;
		
	public int getIdStaff() {
		return idStaff;
	}
	
	public void setName(String name){
		this.fullName = name;
	}
	
	public String getName() {
		return fullName;
	}
		
	public String getAccountName(){
		return accountName;
	}
	
	public void setPassword(String pass) {
		/*MD5Hash md5 = new MD5Hash();
		String md5Pass = md5.md5Java(pass); 
		db.updateInfo("Staff", "idStaff", String.valueOf(idStaff), "password", md5Pass);*/
		this.password = pass;
	}
	
	public String getPassword() {
		//db.readField("Staff", "password", "idStaff", String.valueOf(idStaff));
		return password;
	}
	
	public void setAddress(String address){
		this.address = address;
	}
	
	public String getAddress(){
		return address;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	
	public void updateStaffInfo(){
		ArrayList<String> columnAr = new ArrayList<String>();
		columnAr.add(DBIdentifier.STAFF_NAME);
		columnAr.add(DBIdentifier.STAFF_ADDRESS);
		columnAr.add(DBIdentifier.STAFF_EMAIL);
		columnAr.add(DBIdentifier.STAFF_PHONENUMBER);
		
		ArrayList<String> infoAr = new ArrayList<String>();
		infoAr.add(fullName);
		infoAr.add(address);
		infoAr.add(email);
		infoAr.add(phoneNumber);
		
		ArrayList<String> idColumns = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		
		ArrayList<String> id = new ArrayList<String>();
		id.add(String.valueOf(idStaff));
		
		db = new DBConnector();
		db.updateInfo(DBIdentifier.STAFF_TABLE, idColumns, id, columnAr, infoAr);
		db.close();
	}
	
	public boolean changePassword(){
		db = new DBConnector();
		boolean status = db.updateInfo(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, String.valueOf(idStaff), DBIdentifier.STAFF_PASSWORD, password);
		db.close();
		return status;
	}
	
	public ArrayList<StaffPosition> getAllPositionOfStaff(){
		db = new DBConnector();
		ArrayList<String> columns = new ArrayList<String>();
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		
		columns.add(DBIdentifier.POSITIONKIND_ID);
		columns.add(DBIdentifier.DEPARTMENT_ID);
		idColumns.add(DBIdentifier.STAFF_ID);
		id.add(String.valueOf(idStaff));
		ResultSet rs = db.readFields(DBIdentifier.STAFFPOSITION_TABLE, columns, idColumns, id);
		
		ArrayList<StaffPosition> spArray = new ArrayList<StaffPosition>();
		
		try {
			while(rs.next()){
				StaffPosition sp = new StaffPosition();
				sp.idPositionKind = rs.getInt(DBIdentifier.POSITIONKIND_ID);
				sp.idDepartment = rs.getInt(DBIdentifier.DEPARTMENT_ID);
				spArray.add(sp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
		try {
			if(rs!=null)
				rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return spArray;
	}
	
	public int getMaxLevelPosition(){
		return maxlevelPosition;
	}
	
	public String getPositionOfDepartment(int idDepartment){
		db = new DBConnector();
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		ArrayList<String> getColumns = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		idColumns.add(DBIdentifier.DEPARTMENT_ID);
		id.add(String.valueOf(idStaff));
		id.add(String.valueOf(idDepartment));
		getColumns.add(DBIdentifier.POSITIONKIND_ID);
		ResultSet rs = db.readFields(DBIdentifier.STAFFPOSITION_TABLE, getColumns, idColumns, id);
		String position=null;
		try {
			String idKind = null;
			if(rs.next())
				idKind = rs.getString(DBIdentifier.POSITIONKIND_ID);
			position = db.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_NAME, DBIdentifier.POSITIONKIND_ID, idKind);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return position;
	}
	
	public boolean checkHeadOfDepartment(int idDepartment) {
		db = new DBConnector();
		int idHeadPosition = Integer.parseInt(db.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_ID, DBIdentifier.POSITIONKIND_LEVEL, String.valueOf(1)));
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		ArrayList<String> getColumns = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		idColumns.add(DBIdentifier.POSITIONKIND_ID);
		id.add(String.valueOf(idStaff));
		id.add(String.valueOf(idHeadPosition));
		getColumns.add(DBIdentifier.DEPARTMENT_ID);
		ResultSet rs = db.readFields(DBIdentifier.STAFFPOSITION_TABLE, getColumns, idColumns, id);
		
		try {
			if(rs.next()) {
				int idFound = rs.getInt(DBIdentifier.DEPARTMENT_ID);
				return idFound==idDepartment;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();	
		try {
			if(rs !=null)
				rs.close();
		} catch (SQLException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean addNewStaff(String fullName, String accountName, 
			String password, String address, String email, String phoneNumber){
		db = new DBConnector();
		ArrayList<String> insertColumns = new ArrayList<String>();
		ArrayList<String> insertValues = new ArrayList<String>();
		insertColumns.add(DBIdentifier.STAFF_ACCOUNTNAME);
		insertColumns.add(DBIdentifier.STAFF_ADDRESS);
		insertColumns.add(DBIdentifier.STAFF_EMAIL);
		insertColumns.add(DBIdentifier.STAFF_NAME);
		insertColumns.add(DBIdentifier.STAFF_PASSWORD);
		insertColumns.add(DBIdentifier.STAFF_PHONENUMBER);
		insertColumns.add(DBIdentifier.STAFF_ACTIVEDSTATUS);
		insertValues.add(accountName);
		insertValues.add(address);
		insertValues.add(email);
		insertValues.add(fullName);
		insertValues.add(password);
		insertValues.add(phoneNumber);
		insertValues.add(String.valueOf(0));
		int result = db.insert(DBIdentifier.STAFF_TABLE, insertColumns, insertValues);
		db.close();
		if(result>0)
			return true;
		else
			return false;
	}
	
	public ArrayList<Staff> getAllNotActivedStaff(){
		ArrayList<Staff> staffArray = new ArrayList<Staff>();
		db = new DBConnector();
		
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ACTIVEDSTATUS, String.valueOf(0));
		
		try {
			while(rs.next()) {
				int userId = rs.getInt(DBIdentifier.STAFF_ID);
				StaffFactory factory = new StaffFactory();
				Staff staff = factory.createStaff(userId);
				staffArray.add(staff);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
		try {
			if(rs!=null)
				rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return staffArray;
	}
}
