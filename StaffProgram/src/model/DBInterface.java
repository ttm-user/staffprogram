package model;

import java.sql.ResultSet;
import java.util.ArrayList;

public interface DBInterface {
	public int checkLogin(String accountName, String password);
	public ResultSet readAllInfoOfObject(String table, String idColumn, String id);
	public ResultSet readAllInfoOfTable(String table);
	public ResultSet readAllInfoOfTable(String table, String whereClause);
	public String readField(String table, String column, String idColumn, String id);
	public ResultSet readFields(String table, ArrayList<String> columns, ArrayList<String> idColumns, ArrayList<String> id);
	public boolean updateInfo(String table, ArrayList<String> idField, ArrayList<String> id,
			ArrayList<String> updateColumn, ArrayList<String> updateInfo);
	public boolean updateInfo(String table, String idField, String id,
			String updateColumn, String updateInfo);
	public int insert(String table, ArrayList<String> columns, ArrayList<String> insertValues);
	public void close();
}
