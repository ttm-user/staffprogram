package model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Member extends Staff{

	public Member(){
		
	}
	public Member(String username, String password) {
		db = new DBConnector();
		this.idStaff = Integer.parseInt(db.readField(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, DBIdentifier.STAFF_ACCOUNTNAME, username));
		System.out.println("idStaff:"+idStaff);
		
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, String.valueOf(idStaff));
		try {
			if(rs.next()) {
				this.accountName = rs.getString(DBIdentifier.STAFF_ACCOUNTNAME);
				this.password = rs.getString(DBIdentifier.STAFF_PASSWORD);
				this.fullName = rs.getString(DBIdentifier.STAFF_NAME);
				this.phoneNumber = rs.getString(DBIdentifier.STAFF_PHONENUMBER);
				this.email = rs.getString(DBIdentifier.STAFF_EMAIL);
				this.address = rs.getString(DBIdentifier.STAFF_ADDRESS);
				this.activedStatus = rs.getString(DBIdentifier.STAFF_ACTIVEDSTATUS);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		this.maxlevelPosition = Integer.parseInt(db.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_LEVEL, DBIdentifier.POSITIONKIND_NAME, "member"));
		db.close();
		try { if(rs!=null) rs.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Member(int id) {
		// TODO Auto-generated constructor stub
		db = new DBConnector();
		this.idStaff = id;
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, String.valueOf(idStaff));
		try {
			if(rs.next()) {
				this.accountName = rs.getString(DBIdentifier.STAFF_ACCOUNTNAME);
				this.password = rs.getString(DBIdentifier.STAFF_PASSWORD);
				this.fullName = rs.getString(DBIdentifier.STAFF_NAME);
				this.phoneNumber = rs.getString(DBIdentifier.STAFF_PHONENUMBER);
				this.email = rs.getString(DBIdentifier.STAFF_EMAIL);
				this.address = rs.getString(DBIdentifier.STAFF_ADDRESS);
				this.activedStatus = rs.getString(DBIdentifier.STAFF_ACTIVEDSTATUS);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		this.maxlevelPosition = Integer.parseInt(db.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_LEVEL, DBIdentifier.POSITIONKIND_NAME, "member"));
		db.close();
		try { if(rs!=null) rs.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public int checkAccountNameExist(String username){
		db = new DBConnector();
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ACCOUNTNAME, username);
		try {
			if(rs.next()) return rs.getRow();
			else return 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
		try {
			if(rs != null)
				rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public int checkEmailExist(String email){
		db = new DBConnector();
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_EMAIL, email);
		try {
			if(rs.next()) return rs.getRow();
			else return 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close();
		try {
			if(rs != null)
				rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
}
