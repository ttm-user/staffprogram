package model;
 
import java.sql.*;
import java.util.ArrayList;
 
public class DBConnector implements DBInterface{
	
	private Statement st;
	private Connection connection;
	private ResultSet rs;
	
	public DBConnector() {
		st = null;
		connection = null;
		rs = null;
		loadDriver();	
	}
	
	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/staff","root", "root1234");
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		} 
		return connection;
	}
	
	public void loadDriver(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
		}
	}
	
	public int checkLogin(String accountName, String password) {
		String query = "SELECT COUNT(*) AS rowcount FROM Staff WHERE accountName='"+accountName+"' AND password='"+password+"'";
		
		try {
			connection = getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query);
			rs.next();
			return rs.getInt("rowcount");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return 0;
	}
	
	public ResultSet readAllInfoOfObject(String table, String idColumn, String id){
		String query = "SELECT * FROM "+table+" WHERE "+idColumn+"='"+id+"'";
		System.out.println("read all of object "+ query);
		try {
			connection = getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query);
			if(rs.getRow()==0) {
				System.out.println("not found staff information.");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return rs;
	}
	
	public ResultSet readAllInfoOfTable(String table) {
		String query = "SELECT * FROM "+table;
		System.out.println("read all of table "+table+": "+query);
		try {
			connection = getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query);
			if(rs.getRow()==0) {
				System.out.println("not found department information.");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return rs;
	}
	
	public ResultSet readAllInfoOfTable(String table, String whereClause) {
		String query = "SELECT * FROM "+table+" WHERE "+whereClause;
		System.out.println("read all of table(2)  "+table+": "+query);
		try {
			connection = getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query);
			if(rs.getRow()==0) {
				System.out.println("not found staff information.");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return rs;
	}
	
	public ResultSet readFields(String table, ArrayList<String> columns, ArrayList<String> idColumns, ArrayList<String> id) {
		String query = "SELECT ";
		for(int i=0;i<columns.size();i++){
			query = query +columns.get(i);
			if(i!=(columns.size()-1))
				query = query+",";
		}
		query += " FROM "+table+" WHERE ";
		int i;
		//System.out.println("size:"+idColumns.size());
		for(i=0;i<idColumns.size();i++){
			query += idColumns.get(i)+"='"+id.get(i)+"'";
				query = query+" AND ";
		}
		query = query.substring(0,query.length()-4);
		System.out.println("read fields:"+query);
		try {
			connection = getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query);
			if(rs.getRow()==0)
				System.out.println("not found");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return rs;
	}

	@Override
	public boolean updateInfo(String table, ArrayList<String> idField, ArrayList<String> id,
			ArrayList<String> updateColumn, ArrayList<String> updateInfo) {
		String query = "UPDATE "+table+" SET ";//+updateColumn+"="+updateInfo+" WHERE "+idField+"="+id;
		
		for(int i=0;i<updateColumn.size();i++){
			query = query+updateColumn.get(i)+"='"+updateInfo.get(i)+"'";
			query += ", ";
		}
		query = query.substring(0, query.length()-2);
		query += " WHERE ";
		for(int i=0;i<idField.size();i++){
			query = query+idField.get(i)+"='"+id.get(i)+"'";
			query += " AND ";
		}
		query = query.substring(0, query.length()-4);
		
		System.out.println("updateInfo query:"+query);
		try {
			connection = getConnection();
			st = connection.createStatement();
			if(st.executeUpdate(query)>0)
				return true;
			else
				return false;
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
		return false;
	}

	public boolean updateInfo(String table, String idField, String id,
			String updateColumn, String updateInfo) {
		String query = "UPDATE "+table+" SET "+updateColumn+"='"+updateInfo+"' WHERE "+idField+"='"+id+"'";
		try {
			connection = getConnection();
			st = connection.createStatement();
			if(st.executeUpdate(query)>0)
				return true;
			else return false;
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public int insert(String table, ArrayList<String> columns,
			ArrayList<String> insertValues) {
		String query = "INSERT INTO "+table+" (";
		for(int i=0;i<columns.size();i++){
			query = query+columns.get(i);
			if(i!=(columns.size()-1))
				query = query+",";
		}
		query = query+") VALUES (";
		for(int i=0;i<insertValues.size();i++){
			query = query+"'"+insertValues.get(i)+"',";
		}
		query = query.substring(0,query.length()-1);
		query = query+")";
		System.out.println("insert query:"+query);
		int number = 0;
		try {
			connection = getConnection();
			st = connection.createStatement();
			number = st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
		return number;
	}
	
	public void close() {
	    if (rs != null) try { rs.close(); } catch (SQLException logOrIgnore) {logOrIgnore.printStackTrace();}
	    if (st != null) try { st.close(); } catch (SQLException logOrIgnore) {logOrIgnore.printStackTrace();}
	    if (connection != null) try { connection.close(); } catch (SQLException logOrIgnore) {logOrIgnore.printStackTrace();}
	}

	@Override
	public String readField(String table, String column, String idColumn,
			String id) {
		// TODO Auto-generated method stub
		String query = "SELECT "+column+" FROM "+table+" WHERE "+idColumn+"='"+id+"'";
		String result = null;
		System.out.println("read field query:"+query);
		try {
			connection = getConnection();
			st = connection.createStatement();
			rs = st.executeQuery(query);
			rs.next();
			if(rs.getRow()==0)
				System.out.println("not found");
			else
				result = rs.getString(column);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return result;
	}

}