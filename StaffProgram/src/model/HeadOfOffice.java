package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HeadOfOffice extends Staff {

	public HeadOfOffice(String username, String password) {
		// TODO Auto-generated constructor stub
		db = new DBConnector();
		this.idStaff = Integer.parseInt(db.readField(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, DBIdentifier.STAFF_ACCOUNTNAME, username));
		//System.out.println("idStaff:"+idStaff);
		
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, String.valueOf(idStaff));
		try {
			if(rs.next()) {
				this.accountName = rs.getString(DBIdentifier.STAFF_ACCOUNTNAME);
				this.password = rs.getString(DBIdentifier.STAFF_PASSWORD);
				this.fullName = rs.getString(DBIdentifier.STAFF_NAME);
				this.phoneNumber = rs.getString(DBIdentifier.STAFF_PHONENUMBER);
				this.email = rs.getString(DBIdentifier.STAFF_EMAIL);
				this.address = rs.getString(DBIdentifier.STAFF_ADDRESS);
				this.activedStatus = rs.getString(DBIdentifier.STAFF_ACTIVEDSTATUS);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		this.maxlevelPosition = 1;
		db.close();
		try { if(rs!=null) rs.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public HeadOfOffice(int id) {
		// TODO Auto-generated constructor stub
		db = new DBConnector();
		this.idStaff = id;
		ResultSet rs = db.readAllInfoOfObject(DBIdentifier.STAFF_TABLE, DBIdentifier.STAFF_ID, String.valueOf(idStaff));
		try {
			if(rs.next()) {
				this.accountName = rs.getString(DBIdentifier.STAFF_ACCOUNTNAME);
				this.password = rs.getString(DBIdentifier.STAFF_PASSWORD);
				this.fullName = rs.getString(DBIdentifier.STAFF_NAME);
				this.phoneNumber = rs.getString(DBIdentifier.STAFF_PHONENUMBER);
				this.email = rs.getString(DBIdentifier.STAFF_EMAIL);
				this.address = rs.getString(DBIdentifier.STAFF_ADDRESS);
				this.activedStatus = rs.getString(DBIdentifier.STAFF_ACTIVEDSTATUS);
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		this.maxlevelPosition = 1;
		db.close();
		try { if(rs!=null) rs.close();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void changePositionOfStaff(int idStaff, String department, String newPosition) {
		db = new DBConnector();
		int newIdKind = Integer.parseInt(db.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_ID, DBIdentifier.POSITIONKIND_NAME, newPosition));
		int idDepartment = Integer.parseInt(db.readField(DBIdentifier.DEPARTMENT_TABLE, DBIdentifier.DEPARTMENT_ID, DBIdentifier.DEPARTMENT_NAME, department));
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		ArrayList<String> updateColumns = new ArrayList<String>();
		ArrayList<String> updateInfo = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		idColumns.add(DBIdentifier.DEPARTMENT_ID);
		id.add(String.valueOf(idStaff));
		id.add(String.valueOf(idDepartment));
		updateColumns.add(DBIdentifier.POSITIONKIND_ID);
		updateInfo.add(String.valueOf(newIdKind));

		db.updateInfo(DBIdentifier.STAFFPOSITION_TABLE, idColumns, id, updateColumns, updateInfo);
		
		db.close();
	}
	
	public boolean checkHeadOfDepartment(int idDepartment) {
		db = new DBConnector();
		
		ArrayList<String> idColumns = new ArrayList<String>();
		ArrayList<String> id = new ArrayList<String>();
		ArrayList<String> getColumns = new ArrayList<String>();
		idColumns.add(DBIdentifier.STAFF_ID);
		idColumns.add(DBIdentifier.DEPARTMENT_ID);
		id.add(String.valueOf(idStaff));
		id.add(String.valueOf(idDepartment));
		getColumns.add(DBIdentifier.POSITIONKIND_ID);
		
		ResultSet rs = db.readFields(DBIdentifier.STAFFPOSITION_TABLE, getColumns, idColumns, id);
		
		try {
			if(rs.next()) {
				String idPosition = rs.getString(DBIdentifier.STAFFPOSITION_IDPOSITIONKIND);
				int level = Integer.parseInt(db.readField(DBIdentifier.POSITIONKIND_TABLE, DBIdentifier.POSITIONKIND_LEVEL, DBIdentifier.POSITIONKIND_ID, idPosition));
				if(level==1)
					return true;
				else
					return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
