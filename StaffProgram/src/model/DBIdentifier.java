package model;

public final class DBIdentifier {
	//Table Name
	public static String DEPARTMENT_TABLE="Department";
	public static String STAFF_TABLE="Staff";
	public static String POSITIONKIND_TABLE="PositionKind";
	public static String STAFFPOSITION_TABLE="StaffPosition";
	
	//Column Of Staff table
	public static String STAFF_ID="idStaff";
	public static String STAFF_NAME="fullName";
	public static String STAFF_ACCOUNTNAME = "accountName";
	public static String STAFF_PASSWORD = "password";
	public static String STAFF_ADDRESS = "address";
	public static String STAFF_PHONENUMBER = "phoneNumber";
	public static String STAFF_EMAIL = "email";
	public static String STAFF_ACTIVEDSTATUS = "activedStatus";
	
	//Columns Of Department table
	public static String DEPARTMENT_NAME = "departmentName";
	public static String DEPARTMENT_ID = "idDepartment";
	public static String DEPARTMENT_DESCRIPTION = "description";
	
	//Columns Of PositionKind table
	public static String POSITIONKIND_ID = "idPositionKind";
	public static String POSITIONKIND_NAME = "positionName";
	public static String POSITIONKIND_LEVEL = "level";
	
	//Columns Of StaffPosition table
	public static String STAFFPOSITION_IDSTAFF = "idStaff";
	public static String STAFFPOSITION_IDDEPARTMENT = "idDepartment";
	public static String STAFFPOSITION_IDPOSITIONKIND = "idPositionKind";
	
}
